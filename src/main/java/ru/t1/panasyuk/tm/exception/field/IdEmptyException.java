package ru.t1.panasyuk.tm.exception.field;

public class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error!Id is empty...");
    }

}
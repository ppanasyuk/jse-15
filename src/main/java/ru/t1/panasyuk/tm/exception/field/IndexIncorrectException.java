package ru.t1.panasyuk.tm.exception.field;

public class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}